class CommentsController < ApplicationController
	
	def create
	  @comment = Comment.new(comment_params)
	  @comment.article_id = params[:article_id]

	  @comment.save
	  respond_to do |format|
	  	format.html { redirect_to article_path(@comment.article) }
        format.js
	  end
	  
	end

	def comment_params
	  params.require(:comment).permit(:author_name, :body)
	end
end
